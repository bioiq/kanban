<?php

namespace App\Http\Controllers\API\Login;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;

class LoginController extends AccessTokenController {

    /**
     * Authenticate user and give back Bearer token
     *
     * @param ServerRequestInterface $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(ServerRequestInterface $request) {
        try {
            $tokenResponse = parent::issueToken($request);
            $token = json_decode($tokenResponse->content(), true);

            if (isset($token['error'])) {
                throw new OAuthServerException('The user credentials was incorrect.', 6, 'invalid_credentials', 401);
            }

            return response()->json($token);
        }  catch (ModelNotFoundException $exception) {
            return response()->json(['status' => 'error', 'message' => 'Model not found.'], 404);
        } catch (OAuthServerException $exception) {
            return response()->json(['status' => 'error', 'message' => 'The user credentials was incorrect.'], 401);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'error', 'message' => 'Unexpected error.'], 500);
        }
    }
}

