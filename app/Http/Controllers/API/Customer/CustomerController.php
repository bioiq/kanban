<?php

namespace App\Http\Controllers\API\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRequest;
use App\Models\Customer;

class CustomerController extends Controller {
    public function __construct() {
        $this->middleware(['auth:api', 'role:ProjectManager']);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        $response = Customer::all()->map(
            function (Customer $customer) {
                return [
                    'id' => $customer->id,
                    'name' => $customer->name,
                    'address' => $customer->address,
                    'vatno' => $customer->vatno,
                ];
            }
        );
        return response()->json($response);
    }

    /**
     * @param CustomerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CustomerRequest $request) {
        $data = $request->validated();
        Customer::create($data);
        return response()->json([
            'status' => 'ok',
            'message' => 'Customer creation succeeded',
        ]);
    }

    public function show(Customer $customer) {
        return response()->json($customer);
    }
}
