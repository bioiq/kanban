<?php

namespace App\Http\Controllers\API\Project;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProjectRequest;
use App\Models\Customer;
use App\Models\Project;

class ProjectController extends Controller {
    public function __construct() {
        $this->middleware(['auth:api', 'role:ProjectManager']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Customer $customer) {
        $response = $customer->projects->map(
            function (Project $project) {
                return [
                    'id' => $project->id,
                    'name' => $project->name,
                    'description' => $project->address,
                ];
            }
        );
        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProjectRequest $request
     * @param Customer $customer
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProjectRequest $request, Customer $customer) {
        $data = $request->validated();
        $customer->projects()->create($data);
        return response()->json([
            'status' => 'ok',
            'message' => 'Project creation succeeded',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
    }
}
