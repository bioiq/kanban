<?php

namespace App\Http\Controllers\API\Task;

use App\Http\Controllers\Controller;
use App\Http\Requests\AssignTaskRequest;
use App\Http\Requests\ChangeTaskStatusRequest;
use App\Http\Requests\TaskRequest;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;

class TaskController extends Controller {
    public function __construct() {
        $this->middleware(['auth:api']);
        $this->middleware(['role:ProjectManager'])->only(['index', 'store', 'show', 'assign']);
        $this->middleware(['role:Developer'])->only(['changeStatus']);
    }

    /**
     * @param Project $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Project $project) {
        $response = $project->tasks->map(
            function (Task $task) {
                return [
                    'id' => $task->id,
                    'title' => $task->title,
                    'description' => $task->description,
                    'priority' => $task->priority,
                    'status' => $task->status,
                ];
            }
        );
        return response()->json($response);
    }

    /**
     * @param TaskRequest $request
     * @param Project $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TaskRequest $request, Project $project) {
        $data = $request->validated();
        $project->tasks()->create($data);
        return response()->json([
            'status' => 'ok',
            'message' => 'Task creation succeeded',
        ]);
    }

    /**
     * @param Task $task
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Task $task) {
        return response()->json($task);
    }

    public function assignTaskToDev(AssignTaskRequest $request, Task $task) {
        $assigned_dev = User::find($request->user_id);

        $task->user()->associate($assigned_dev)->save();
        return response()->json([
            'status' => 'ok',
            'message' => 'Task assignment succeeded',
        ]);
    }

    public function changeTaskStatus(ChangeTaskStatusRequest $request, Task $task) {
        $data = $request->validated();
        $task->update(['status' => $data['status']]);
        return response()->json([
            'status' => 'ok',
            'message' => 'Task assignment succeeded',
        ]);
    }
}
