<?php

namespace Tests\Feature;

use App\Models\Customer;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class CustomerTest extends TestCase {
    use RefreshDatabase;

    private $user;

    protected function setUp(): void {
        parent::setUp();

        $role = Role::create(['name' => 'ProjectManager']);
        $this->user = User::factory()->create();
        $this->user->assignRole($role);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCustomerWillBeCreated() {
        $customer = Customer::create([
            'name' => 'TestCustName',
            'address' => 'TestCustAddress',
            'vatno' => 'TestCustVat',
        ]);
        $this->assertCount(1, Customer::all());
        $this->assertEquals('TestCustName', $customer->name);
        $this->assertEquals('TestCustAddress', $customer->address);
        $this->assertEquals('TestCustVat', $customer->vatno);
    }

    public function testCustomerWillNotBeCreatedIfNotAuthenticated() {
        $this->postJson('/api/customers', [
            'name' => 'TestCustName',
            'address' => 'TestCustAddress',
            'vatno' => 'TestCustVat',
        ])->assertStatus(401);
    }

    public function testCustomerWillBeCreatedIfAuthenticated() {
        $this->actingAs($this->user, 'api');

        $this->postJson('/api/customers', [
            'name' => 'TestCustName',
            'address' => 'TestCustAddress',
            'vatno' => 'TestCustVat',
        ])->assertStatus(200);
        $this->assertCount(1, Customer::all());
    }

    public function testCustomerWillBeIndex() {
        $this->actingAs($this->user, 'api');
        $customer = Customer::factory()->create();
        $this->getJson('/api/customers')
            ->assertStatus(200)
            ->assertJsonStructure([[
                'id',
                'name',
                'address',
                'vatno'
            ]]);
    }

    public function testCustomerWillBeShow() {
        $this->actingAs($this->user, 'api');
        $customer = Customer::factory()->create();
        $this->get('/api/customers/' . $customer->id )
            ->assertStatus(200)
            ->assertJsonStructure([
                'id',
                'name',
                'address',
                'vatno'
            ]);
    }
}
