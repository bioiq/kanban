<?php

namespace Tests\Feature;

use App\Models\Customer;
use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class ProjectTest extends TestCase {
    use RefreshDatabase;

    private $user;
    private $customer;

    protected function setUp(): void {
        parent::setUp();

        $role = Role::create(['name' => 'ProjectManager']);
        $this->user = User::factory()->create();
        $this->customer = Customer::factory()->create();
        $this->user->assignRole($role);
    }

    public function testProjectWillBeCreated() {
        //se lo uso come metodo crea una nuova relazione, se lo uso come proprietà vedo la relazione esistente
        //ricorda che l'ultima cosa scritta è quella che mi viene tornata in questo caso create di project e POI viene associato a customer
        $project = $this->customer->projects()->create([
            'name' => 'testProjName',
            'description' => 'testDesc',
        ]);

        $this->assertCount(1, Project::all());
        $this->assertEquals($this->customer->projects->first()->id, $project->id);
        $this->assertEquals($project->customer->id, $this->customer->id);
    }

    public function testProjectWillNotBeCreatedIfNotAuthenticated() {
        $this->postJson('/api/customers/' . $this->customer->id . '/projects', [
            'name' => 'testProjName',
            'description' => 'testDesc',
        ])->assertStatus(401);
    }

    public function testProjectWillBeCreatedIfAuthenticated() {
        $this->actingAs($this->user, 'api');

        $this->postJson('/api/customers/' . $this->customer->id . '/projects', [
            'name' => 'testProjName',
            'description' => 'testDesc',
        ])->assertStatus(200);
        $this->assertCount(1, Project::all());
    }

    public function testProjectWillBeIndex() {
        $this->actingAs($this->user, 'api');
        $project = Project::factory()->for($this->customer)->create();
        $this->getJson('/api/customers/' . $this->customer->id . '/projects')->assertStatus(200)->assertJsonStructure([[
            'id',
            'name',
            'description',
        ]]);
    }
}
