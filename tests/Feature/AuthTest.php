<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\ClientRepository;
use Tests\TestCase;

class AuthTest extends TestCase {
    use RefreshDatabase;

    private $user;
    private $client;

    protected function setUp(): void {
        parent::setUp();

        $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();

        $client = (new ClientRepository)->createPasswordGrantClient(null, 'Personal Access Client', 'http://localhost');
        $this->client = $client->find(1);

        $this->user = User::factory()->create();
    }

    public function testLogin() {
        $response = $this->postJson('/api/login', [
           'grant_type' => 'password',
           'client_id' => $this->client->id,
           'client_secret' => $this->client->secret,
           'username' => $this->user->email,
           'password' => 'password',
        ])->assertOk();

        $data = $response->json();

        $this->assertArrayHasKey('token_type', $data);
        $this->assertArrayHasKey('expires_in', $data);
        $this->assertArrayHasKey('access_token', $data);
        $this->assertArrayHasKey('refresh_token', $data);
    }
}
