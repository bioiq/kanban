<?php

namespace Tests\Feature;

use App\Models\Customer;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class TaskTest extends TestCase {
    use RefreshDatabase;

    private $user;
    private $customer;
    private $project;
    private $role;

    protected function setUp(): void {
        parent::setUp();

        $this->role = Role::create(['name' => 'ProjectManager']);
        $this->user = User::factory()->create();
        $this->customer = Customer::factory()->create();
        $this->project = Project::factory()->for($this->customer)->create();
    }

    public function testTaskWillBeCreated() {
        $this->project->tasks()->create([
            'title' => 'testTaskTitle',
            'description' => 'testTaskDescription',
            'priority' => 'testTaskPriority',
            'status' => 'testTaskStatus',
        ]);
        $this->assertCount(1, Task::all());
    }

    public function testTaskWillNotBeCreatedIfNotAuthenticated() {
        $this->user->assignRole($this->role);
        $this->postJson('/api/projects/' . $this->project->id . '/tasks', [
            'title' => 'testTaskTitle',
            'description' => 'testTaskDescription',
            'priority' => 'testTaskPriority',
            'status' => 'testTaskStatus',
        ])->assertStatus(401);
    }

    public function testTaskWillBeCreatedIfAuthenticated() {
        $this->user->assignRole($this->role);
        $this->actingAs($this->user, 'api');

        $this->postJson('/api/projects/' . $this->project->id . '/tasks', [
            'title' => 'testTaskTitle',
            'description' => 'testTaskDescription',
            'priority' => 'testTaskPriority',
            'status' => 'testTaskStatus',
        ])->assertStatus(200);
        $this->assertCount(1, Task::all());
    }

    public function testTaskWillBeIndex() {
        $this->user->assignRole($this->role);
        $this->actingAs($this->user, 'api');
        $project = Project::factory()->for($this->customer)->create();
        $this->getJson('/api/projects/' . $this->project->id . '/tasks')->assertStatus(200);
    }

    public function testTaskWillBeShow() {
        $this->user->assignRole($this->role);
        $this->actingAs($this->user, 'api');
        $task = Task::factory()->for($this->project)->create();
        $this->get('/api/tasks/' . $task->id)
            ->assertStatus(200)
            ->assertJsonStructure([
                'title',
                'description',
                'priority',
                'status',
            ]);
    }


    public function testProjectManagerCanAssignTask() {
        $this->user->assignRole($this->role);
        $this->actingAs($this->user, 'api');
        $task = Task::factory()->for($this->project)->create();
        $dev = User::factory()->create();
        $task->user()->associate($dev)->save();
        $this->assertEquals($dev->id, $task->user_id);
    }
    //creare un test per testare la route che mi permette di assegnare il dev al task
    public function testRouteAssignment() {
        $this->user->assignRole($this->role);
        $this->actingAs($this->user, 'api');
        $task = Task::factory()->for($this->project)->create();
        $developer = User::factory()->create();
        $this->patchJson('/api/tasks/' . $task->id . '/assign', [
            'user_id' => $developer->id,
        ]);
        $task->refresh();
        $this->assertNotNull($task->user_id);
    }

    public function testDevCanChangeStatus(){
        $role = Role::create(['name' => 'Developer']);
        $this->user->assignRole($role);
        $this->actingAs($this->user, 'api');
        $task = $this->project->tasks()->create([
            'title' => 'testTaskTitle',
            'description' => 'testTaskDescription',
            'priority' => 'testTaskPriority',
            'status' => 'testTaskStatus',
        ]);
        $response = $this->patchJson('/api/tasks/' . $task->id . '/change-status', [
            'status' => 'high',
        ]);
        $task->refresh();
        $this->assertEquals('high', $task->status);
    }
}
