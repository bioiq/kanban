<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [\App\Http\Controllers\API\Login\LoginController::class, 'login']);

Route::post('/customers', [\App\Http\Controllers\API\Customer\CustomerController::class, 'store']);
Route::get('/customers', [\App\Http\Controllers\API\Customer\CustomerController::class, 'index']);
Route::get('/customers/{customer}', [\App\Http\Controllers\API\Customer\CustomerController::class, 'show']);

Route::post('/customers/{customer}/projects', [\App\Http\Controllers\API\Project\ProjectController::class, 'store']);
Route::get('/customers/{customer}/projects', [\App\Http\Controllers\API\Project\ProjectController::class, 'index']);
Route::get('/projects/{project}', [\App\Http\Controllers\API\Customer\CustomerController::class, 'show']);

Route::post('/projects/{project}/tasks', [\App\Http\Controllers\API\Task\TaskController::class, 'store']);
Route::get('/projects/{project}/tasks', [\App\Http\Controllers\API\Task\TaskController::class, 'index']);
Route::get('/tasks/{task}', [\App\Http\Controllers\API\Task\TaskController::class, 'show']);
Route::patch('/tasks/{task}/assign', [\App\Http\Controllers\API\Task\TaskController::class, 'assignTaskToDev']);
Route::patch('/tasks/{task}/change-status', [\App\Http\Controllers\API\Task\TaskController::class, 'changeTaskStatus']);

